# Debian on btrfs on PC
Starting as a clone of my other repository [here](https://gitlab.com/JohnCorbs/macbookpro-13-3). This repository will show how I myself have gotten debian functional onto my pc like my macbook repo, while also having setup everyting to my preferences :)

[TOC]

## prerequisits:

**Have a existing instalce of linux with btrfs-progs, debootstrap, and an internet connection**
a link to my guide on creating such usb can be found [here](https://gitlab.com/-/snippets/3603802)

## Install

### partitions ur disk & mount:
```
EFI partition:
    ef00 type
    mkfs.fat -F 32 /dev/nvme0n1p1
Partition to btrfs:
    8300 type
    mkfs.btrfs /dev/nvme0n1p2
```
```
mount /dev/nvme0n1p2 /mnt
cd /mnt
mkdir debian
btrfs subvolume create debian/@root
btrfs subvolume create debian/@home
btrfs subvolume create debian/@swap
btrfs subvolume create debian/@snapshots

cd
umount /mnt

mount /dev/nvme0n1p2 -o compress=zstd:2,subvol=debian/@root /mnt
mkdir -p /mnt/{home,.snapshots,.swap,boot/efi}
mount /dev/nvme0n1p2 -o compress=zstd:2,subvol=debian/@home /mnt/home
mount /dev/nvme0n1p2 _o compress=zstd:10,subvol=debian/@snapshots /mnt/.snapshots
mount /dev/nvme0n1p2 -o compress=none,subvol=debian/@swap /mnt/swap

mount /dev/nvme0n1p1 /mnt/boot/efi
```
### install system:
`debootstrap --arch amd64 stable /mnt https://deb.debian.org/debian`

```
LANG=C.UTF-8 chroot /mnt /bin/bash
```
add the following to the single line in `/etc/apt/sources.list`:
`contrib non-free non-free-firmware`

install packages:
```
apt update
apt install linux-image-amd64 vim amd64-microcode locales firmware-linux make gcc git libx11-dev libxft-dev libxinerama-dev xorg sudo systemd-resolved btrfs-progs gdisk usbutils

/usr/sbin/dpkg-reconfigure tzdata
/usr/sbin/dpkg-reconfigure locales
/usr/sbin/locale-gen
```
`btrfs filesystem mkswapfile --size 32G /.swap/swapfile`
exit chroot then run 
```
swapon /mnt/.swap/swapfile
genfstab -U /mnt >> /mnt/etc/fstab
```
edit fstab to look like the following: PLEASE REMEMVER TO FIX !!!!!
```
# UNCONFIGURED FSTAB FOR BASE SYSTEM
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# /dev/nvme0n1p2
UUID=25fdec5a-db0c-4925-8b62-c9df8d27ca59	/         	btrfs     	rw,relatime,compress=zstd:2,ssd,discard=async,space_cache=v2,subvolid=256,subvol=/debian/@root	0 0

# /dev/nvme0n1p1
UUID=A6A8-947B      	/boot/efi 	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro	0 2

# /dev/nvme0n1p2
UUID=25fdec5a-db0c-4925-8b62-c9df8d27ca59	/home     	btrfs     	rw,relatime,compress=zstd:2ssd,discard=async,space_cache=v2,subvolid=257,subvol=/debian/@home	0 0

# /dev/nvme0n1p2
UUID=25fdec5a-db0c-4925-8b62-c9df8d27ca59	/.swap    	btrfs     	rw,relatime,compress=none,ssd,discard=async,space_cache=v2,subvolid=258,subvol=/debian/@swap	0 0

# /dev/nvme0n1p2
UUID=25fdec5a-db0c-4925-8b62-c9df8d27ca59	/.snapshots	btrfs     	rw,relatime,compress=zstd:10,ssd,discard=async,space_cache=v2,subvolid=259,subvol=/debian/@snapshots	0 0

# /dev/nvme0n1p3
UUID=3d463fb3-3f53-4cb4-af93-fd6a14df66b8	none      	swap      	defaults  	0 0

/.swap/swapfile     	none      	swap      	defaults  	0 0

```

git clone my rEFInd repo [here](https://gitlab.com/JohnCorbs/refind) and setup accordingly, details in repo

```
passwd
useradd -m user
passwd user
```
`sytemctl enable systemd-netwokd systemd-resolved`

`vim /etc/systemd/network/20-wired.network`
```
[Match]
Name=enp5s0
[Network]
DHCP=yes
IgnoreCarrierLoss=3s
```

`efibootmgr -c -d /dev/nvme0n1p1 -l \\refind\\refind_x64.efi -L "rEFInd"`

## Post Install:

### dwm and st install
### ly
### wallpaper
###application manager

## Things to workout/cover:
- [ ] dwm
- [ ] suspend & hybernate
- [ ] multi-monitor
- [ ] 

## Relevant links (general)
| links |
| :--- |
| https://gitlab.com/JohnCorbs/debianpc/-/blob/main/README.md?ref_type=heads |
| https://gitlab.com/JohnCorbs/dwm |
| https://gitlab.com/JohnCorbs/st |
| https://suckless.org |

